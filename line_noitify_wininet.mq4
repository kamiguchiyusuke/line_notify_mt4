#property copyright ""
#property link      ""
#property version   "1.00"
#property strict

#define DEFAULT_HTTPS_PORT     443
#define SERVICE_HTTP   3
#define FLAG_SECURE            0x00800000  // use PCT/SSL if applicable (HTTP)
#define FLAG_PRAGMA_NOCACHE    0x00000100  // asking wininet to add "pragma: no-cache"
#define FLAG_KEEP_CONNECTION   0x00400000  // use keep-alive semantics
#define FLAG_RELOAD            0x80000000  // retrieve the original item

extern string TOKEN = "";                    //アクセストークン
extern string line_message = "Hello World";  //通知内容


#import "wininet.dll"
int InternetOpenW(string sAgent, int lAccessType, string sProxyName, string sProxyBypass, int lFlags);
int InternetConnectW(int hInternet, string ServerName, int nServerPort, string lpszUsername, string lpszPassword, int dwService, int dwFlags, int dwContext);
int HttpOpenRequestW(int hConnect, string Verb, string ObjectName, string Version, string Referer, string AcceptTypes, int dwFlags, int dwContext);
int HttpSendRequestW(int hRequest, string &lpszHeaders, int dwHeadersLength, uchar &lpOptional[], int dwOptionalLength);
int InternetReadFile(int hFile, uchar &sBuffer[], int lNumBytesToRead, int &lNumberOfBytesRead);
int InternetCloseHandle(int hInet);
#import


int OnInit()
{
  if (MQLInfoInteger(MQL_DLLS_ALLOWED) != 1) {
    Print("==================");
    Print("DLL is not allowed");
    Print("==================");
    return(INIT_FAILED);
  }

  Linenotify(TOKEN, line_message);
  return(INIT_SUCCEEDED);
}

void OnTick(){

}


void Linenotify(string token, string message){

  string headers;
  int session = 0; 
  int connect = 0;
  int hRequest, hSend;
  uchar post[];

  ArrayResize(post, StringToCharArray("message=" + message, post, 0, WHOLE_ARRAY, CP_UTF8) - 1);

  string UserAgent = "MetaTrader 4 Terminal";
  string nill = "";


  session = InternetOpenW(UserAgent, 0, nill, nill, 0);
  if(session <= 0){
    if(session > 0) InternetCloseHandle(session); session = -1;
    if(connect > 0) InternetCloseHandle(connect); connect = -1;
    Print("Err CreateSession");
  }

  string host = "notify-api.line.me";
  connect = InternetConnectW(session, host, DEFAULT_HTTPS_PORT, nill, nill, SERVICE_HTTP, 0, 0);
  if(connect <= 0){
    if(session > 0) InternetCloseHandle(session); session = -1;
    if(connect > 0) InternetCloseHandle(connect); connect = -1;
    Print("Err create Connect");
  }


  string Vers    = "HTTP/1.1";
  string POST    = "POST";
  string Object  = "/api/notify";
  hRequest = HttpOpenRequestW(connect, POST, Object, Vers, NULL, NULL, (int)(FLAG_SECURE|FLAG_KEEP_CONNECTION|FLAG_RELOAD|FLAG_PRAGMA_NOCACHE), 0);
  if(hRequest <= 0){
    if(session > 0) InternetCloseHandle(session); session = -1;
    if(connect > 0) InternetCloseHandle(connect); connect = -1;
    Print("Err OpenRequest");
  }

  headers = "Authorization: Bearer " + token + "\r\n";
  headers += "Content-Type: application/x-www-form-urlencoded";
  hSend = HttpSendRequestW(hRequest, headers, StringLen(headers), post, ArraySize(post));


  if(hSend <= 0){
    Print("Err SendRequest");
    if(connect > 0) InternetCloseHandle(hRequest);
    if(session > 0) InternetCloseHandle(session);  session  = -1;
    if(connect > 0) InternetCloseHandle(connect);  connect  = -1;
  }else{
    Print("send OK!");
  }

  InternetCloseHandle(hSend);
  InternetCloseHandle(hRequest);
  if(session > 0) InternetCloseHandle(session); session = -1;
  if(connect > 0) InternetCloseHandle(connect); connect = -1;
}
